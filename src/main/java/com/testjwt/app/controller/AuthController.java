package com.testjwt.app.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nimbusds.jose.shaded.json.JSONObject;
import com.testjwt.app.dao.RoleDao;
import com.testjwt.app.dao.UserDao;
import com.testjwt.app.entity.AllRoles;
import com.testjwt.app.entity.Role;
import com.testjwt.app.entity.User;
import com.testjwt.app.payload.request.LoginRequest;
import com.testjwt.app.payload.response.MessageResponse;
import com.testjwt.app.security.jwt.JwtUtils;
import com.testjwt.app.security.services.UserDetailsImpl;

@CrossOrigin(origins = "http://localhost:4200/", maxAge = 3600, allowCredentials = "true")
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	
	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	RoleDao roleDao;
	
	@Autowired
	PasswordEncoder encoder;
	
	@Autowired
	JwtUtils jwtUtils;

	
	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest, HttpServletResponse response) {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

		// authentification
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		//Window.getWindows().

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities()
				.stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());
				
		
		// Add a session cookie
		final Cookie cookieJwt = new Cookie("token", jwt);		
        cookieJwt.setHttpOnly(true);
        cookieJwt.setMaxAge(900);
        cookieJwt.setPath("/api");
        response.addCookie(cookieJwt);  
        response.addHeader("token", jwt);                
        //end
        
        //retrieve datas from user
        List<JSONObject> jsonDataUser = new ArrayList<JSONObject>();
        JSONObject jsonData = new JSONObject();
        jsonData.put("token", jwt);
        jsonData.put("id", userDetails.getId());
        jsonData.put("email", userDetails.getEmail());
        jsonData.put("roles", roles);
        jsonDataUser.add(jsonData);
        //send
        return new ResponseEntity<List<JSONObject>>(jsonDataUser, HttpStatus.OK);		
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody ObjectNode objectNode) {

		if (userDao.existsByEmail(objectNode.get("email").asText())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
		}

		// Create new user's account

		User user = new User(objectNode.get("lastName").asText(), objectNode.get("firstName").asText(), objectNode.get("email").asText(),
				encoder.encode(objectNode.get("password").asText()));

		String stringRole = objectNode.get("role").asText();
		Set<String> strRoles = new HashSet<>();
		strRoles.add(stringRole);
		Set<Role> roles = new HashSet<>();

		if (stringRole.isEmpty()) {
			Role userRole = roleDao.findByName(AllRoles.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {

				switch (role) {
				case "admin":
					Role adminRole = roleDao.findByName(AllRoles.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);
					break;

				case "moderator":
					Role modRole = roleDao.findByName(AllRoles.ROLE_MODERATOR)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(modRole);
					break;
				default:
					Role userRole = roleDao.findByName(AllRoles.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}

			});
		}
		user.setRoles(roles);
		userDao.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}
	
	@PostMapping("/user/find")
	public Optional<User> findUser(@RequestBody String email) {		
		return userDao.findByEmail(email);		
	}

}
