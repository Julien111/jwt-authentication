package com.testjwt.app.security.services;

import javax.transaction.Transactional;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.testjwt.app.dao.UserDao;
import com.testjwt.app.entity.User;
import com.testjwt.app.exception.EmailNotFoundException;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UserDao userDao;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String email) throws EmailNotFoundException {
		User user = userDao.findByEmail(email)
				.orElseThrow(() -> new EmailNotFoundException("User Not Found with email: " + email));

		return UserDetailsImpl.build(user);
	}
}
