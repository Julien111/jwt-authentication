package com.testjwt.app.exception;

public class SignatureException extends RuntimeException {

	/**
	 * pas utile (à supprimer)
	 */
	private static final long serialVersionUID = 1L;

	public SignatureException() {
		super();		
	}

	public SignatureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);		
	}

	public SignatureException(String message, Throwable cause) {
		super(message, cause);		
	}

	public SignatureException(String message) {
		super(message);		
	}

	public SignatureException(Throwable cause) {
		super(cause);		
	}
	
	

}
