package com.testjwt.app.entity;

public enum AllRoles {
	ROLE_USER,
	ROLE_MODERATOR,
	ROLE_ADMIN
}
