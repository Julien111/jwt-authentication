package com.testjwt.app.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.testjwt.app.security.jwt.JwtUtils;

@CrossOrigin(origins = "http://localhost:4200/", maxAge = 3600, allowCredentials = "true")
@RestController
@RequestMapping("/api/test")
public class TestController {
	
	@Autowired
	JwtUtils jwtUtils;
	
	@GetMapping("/all")
	public String allAccess() {
		return "Public content all";
	}
	
	@GetMapping("/user")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public ResponseEntity<String> userAccess(HttpServletRequest request) {		
		String token = jwtUtils.getCookieValue(request);
		if(!jwtUtils.validateJwtToken(token)) {
			return ResponseEntity.ok("No !!!");
		}
		return ResponseEntity.ok("User Content");
	}
	
	@GetMapping("/mod")
	@PreAuthorize("hasRole('MODERATOR')")
	public ResponseEntity<String> moderatorAccess() {
		return ResponseEntity.ok("Moderator Board.");
	}

	@GetMapping("/admin")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<String> adminAccess() {
		return ResponseEntity.ok("Admin Board.");
	}

}
//find cookie
//decode JWT
//DecodedJWT decodedJWT = JWT.decode(jwtString);
