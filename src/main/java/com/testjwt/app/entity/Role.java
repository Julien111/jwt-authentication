package com.testjwt.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="roles")
public class Role {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;	
	
	@Enumerated(EnumType.STRING)
	@Column(length=20)
	private AllRoles name;	

	public Role() {
		
	}	

	public Role(AllRoles name) {
		super();
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AllRoles getName() {
		return name;
	}

	public void setName(AllRoles name) {
		this.name = name;
	}	

}
