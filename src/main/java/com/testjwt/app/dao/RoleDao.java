package com.testjwt.app.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.testjwt.app.entity.AllRoles;
import com.testjwt.app.entity.Role;

public interface RoleDao extends JpaRepository<Role, Long> {
	Optional<Role> findByName(AllRoles name);
}
