package com.testjwt.app.security.jwt;

import java.util.Arrays;
import java.util.Date;

import javax.crypto.SecretKey;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.testjwt.app.security.services.UserDetailsImpl;

import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.io.Encoders;
import io.jsonwebtoken.security.Keys;

@Component
public class JwtUtils {

	private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

	private final SecretKey jwtSecret = Keys.secretKeyFor(SignatureAlgorithm.HS256);
	
	private final String secretString = Encoders.BASE64.encode(this.jwtSecret.getEncoded());

	@Value("${testjwt.app.jwtExpirationMs}")
	private int jwtExpirationMs;
	

	public String generateJwtToken(Authentication authentication) {
		UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();
		
		System.out.println(userPrincipal.getEmail());
		
		return Jwts.builder()
				.setSubject((userPrincipal.getEmail()))
				.setIssuedAt(new Date())
				.setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))				
				.signWith(jwtSecret)
				.compact();
	}

	public String getEmailFromJwtToken(String token) {
		SecretKey secret = Keys.hmacShaKeyFor(Decoders.BASE64.decode(secretString));
		return Jwts.parserBuilder().setSigningKey(secret).build().parseClaimsJws(token).getBody().getSubject();
	}

	public boolean validateJwtToken(String authToken) {
		if(authToken == null) {
			return false;
		}
		try {
			SecretKey secret = Keys.hmacShaKeyFor(Decoders.BASE64.decode(secretString));
			Jwts.parserBuilder().setSigningKey(secret).build().parseClaimsJws(authToken);
			return true;
		} catch (JwtException e) {
			logger.error("Invalide JWT token: {}", e.getMessage());
		}
		return false;
	}
	
	public String getCookieValue(HttpServletRequest req) {
	    return Arrays.stream(req.getCookies())
	            .filter(c -> c.getName().equals("token"))
	            .findFirst()
	            .map(Cookie::getValue)
	            .orElse(null);
	}

}
